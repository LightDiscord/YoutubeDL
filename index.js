#!/usr/bin/env node

const Inquirer = require('inquirer')
const ytdl = require('ytdl-core')
const fs = require('fs')

const questions = [{
        type: 'input',
        name: 'video_id',
        message: 'Quel est l\'id de la vidéo ?'
    },
    {
        type: 'confirm',
        name: 'video_songonly',
        message: 'Télécharger uniquement le son ?',
        default: false
    }
]

Inquirer.prompt(questions).then(answers => {
    const loader = { states: ["⣾", "⣽", "⣻", "⢿", "⡿", "⣟", "⣯", "⣷"], actual: 8 }
    const BottomBar = new Inquirer.ui.BottomBar({ bottomBar: loader.states[loader.actual % 8] + " Téléchargement en cours..." });
    setInterval(function() {
        BottomBar.updateBottomBar(loader.states[loader.actual++ % 8] + " Téléchargement en cours...")
    }, 100)

    const options = {}
    if (answers.video_songonly) Object.assign(options, { filter: 'audioonly' })

    const video = ytdl(formatUrl(answers.video_id), options)

    video.on('info', (info, format) => {
        video.pipe(fs.createWriteStream(`${info.author.name} - ${info.title}.mp${formatType(answers.video_songonly)}`))
    })

    video.on('finish', _ => {
        BottomBar.updateBottomBar('✔ Téléchargement terminé !\n')
        process.exit(0)
    })
})

function formatUrl(id) {
    return id.includes('youtube.com') ? id : `https://www.youtube.com/watch?v=${id}`
}

function formatType(audioOnly) {
    return audioOnly ? '3' : '4'
}